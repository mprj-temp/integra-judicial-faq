$(document).ready(function () {

  /* $('#navbar ul').onePageNav({
     currentClass: 'current',
     changeHash: false,
     scrollSpeed: 750,
     scrollThreshold: 0,
     filter: '',
     easing: 'swing',
     begin: function () {
     },
     end: function () {
     },
     scrollChange: function ($currentListItem) {
     }
   });*/

  var scrollLink = $('.scroll');

  // Smooth scrolling
  scrollLink.click(function (e) {
    e.preventDefault();
    $('body,html').animate({
      scrollTop: $(this.hash).offset().top
    }, 1000);
  });

  // Active link switching
  $(window).scroll(function () {
    var scrollbarLocation = $(this).scrollTop();

    scrollLink.each(function () {

      var sectionOffset = $(this.hash).offset().top - 0;

      if (sectionOffset <= scrollbarLocation) {
        $(this).parent().addClass('current');
        $(this).parent().siblings().removeClass('current');
      }
    })

  })


});
