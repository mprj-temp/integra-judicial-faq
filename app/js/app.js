
const navbar = () => {
  let navbar = document.querySelector('.navbar');
  let logo = document.querySelector("nav > a > img#logo");
  logo.src = 'img/mprj-integra-horizontal.svg';
  let last_known_scroll_position = 0;
  let last_document_width = !0;
  let ticking = false;

  const toggleLogoColor = (estado) => {
    return logo.src = estado ? 'img/mprj-integra-horizontal.svg' : 'img/mprj-integra-horizontal-negativo.svg';
  };

  const showNavBar = () => {
    if (window.scrollY <= 40) {
      navbar.classList.remove('navbar-scroll');
      toggleLogoColor(true);
    } else {
      navbar.classList.add('navbar-scroll');
      toggleLogoColor(false);
    }
  };

  const blah = () => {
    document.body.clientWidth <= 990  ? toggleLogoColor(false) : toggleLogoColor(true);
  };



  window.addEventListener('scroll', function () {
    last_known_scroll_position = window.scrollY;
    if (!ticking) {
      window.requestAnimationFrame(function () {
        showNavBar(last_known_scroll_position);
        ticking = false;
      });
      ticking = true;
    }
  });

  window.addEventListener("resize", () => {
    last_document_width = document.body.clientWidth;
    if (!ticking) {
      window.requestAnimationFrame(() => {
        blah(last_document_width);
        ticking = false;
      });
      ticking = true;
    }
  });

  if (document.body.clientWidth !== 990) blah();
  if (window.scrollY !== 0) showNavBar();
};




const preloader = () => {
  Document.prototype.ready = _callback => {
    if (_callback && typeof _callback === 'function') {
      document.addEventListener("DOMContentLoaded", () => {
        if (document.readyState === "interactive" || document.readyState === "complete") {
          return _callback();
        }
      });
    }
  };

  document.ready(() => {
    const main = document.querySelector("#main");
    const preloader = document.querySelector('#page-preloader');
    const product_text = document.querySelector("#home > div .product-text");
    const product_image = document.querySelector("#home > div .product-image");

    preloader.classList.add('animated', 'fast', 'fadeOut');
    main.classList.add('animated', 'delay-2s', 'fadeIn');
    product_text.classList.add('animated', 'delay-2s', 'fadeInLeft');
    product_image.classList.add('animated', 'delay-2s', 'fadeInRight');

    preloader.style.display = 'none';

    init();

  });
};


const init = () => {
  navbar();
};

preloader();

