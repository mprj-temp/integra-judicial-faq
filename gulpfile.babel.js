/**
 * @project:  integra-judicial-faq
 * @author:   Tiago de Araujo Fernande - TAF (@Tiago de Araujo Fernande - TAF)
 * Copyright (c) 2018 Tiago de Araujo Fernande - TAF
 * Released under the ISC license
 */

import gulp from 'gulp';
import del from 'del';
import browserSync from 'browser-sync';
import browserify from 'browserify';
import babelify from 'babelify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import cleanCSS from 'gulp-clean-css';
import gulpLoadPlugins from 'gulp-load-plugins';
import {argv} from 'yargs';
import config from './config.json'
import pkg from './package.json';

const $ = gulpLoadPlugins();
const server = browserSync.create();
const banner = `
/**
 * @project:  ${pkg.name}
 * @author:   ${pkg.author}
 * Copyright (c) ${(new Date()).getFullYear()} ${pkg.author}
 * Released under the ${pkg.license} license
*/
`;

/* List all tasks and subtasks */
gulp.task('help', $.taskListing);


const clean = () => del(['dist']);

const styles = () => {
  return gulp.src(config.sass_src)
    .pipe($.plumber())
    .pipe($.if(argv.pretty, $.sourcemaps.init()))
    .pipe($.sass({
      includePaths: [
        '/node_modules/@fortawesome/fontawesome-free/scss/',
        '/node_modules/bootstrap',
        '/node_modules/animate.css/'
      ]
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer(config.autoprefixer_options))
    .pipe(cleanCSS())
    .pipe($.size({title: 'Styles'}))
    .pipe($.header(banner, {pkg}))
    .pipe($.concat(config.css_file_name))
    .pipe($.if(argv.pretty, $.sourcemaps.write('./')))
    .pipe(gulp.dest(config.sass_dest))
    .pipe(server.stream())
};

const scripts = () => {
  let b = browserify({
    entries: './app/js/app.js',
    debug: true,
  }).transform(babelify, {presets: ["@babel/preset-env"]});
  return b.bundle()
    .pipe(source('./app/js/app.js'))
    .pipe(buffer())
    .pipe($.size({title: 'Bundle scripts'}))
    .pipe($.if(argv.pretty, $.sourcemaps.init()))
    .pipe($.concat(config.js_file_name))
    .pipe($.if(!argv.pretty, $.uglify({})))
    .pipe($.header(banner, {pkg}))
    .pipe($.size({title: 'Scripts'}))
    .pipe(gulp.dest(config.js_dest))
};

const vendor = () => {
  return gulp.src([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
    './app/js/jqueryNav.js',
    './app/js/popper.min.js',
    './app/js/parallax.min.js',
    './app/js/app.vendor.js',
  ])
    .pipe(gulp.dest(config.js_vendor_dest))
    .pipe($.size({title: 'Vendor js'}))
    .pipe(browserSync.stream());
};


const views = () => {
  return gulp.src(config.templates_src)
    .pipe($.plumber())
    .pipe($.pug(
      {
        pretty: true,
        self: true
      }
    ))
    .pipe($.htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest(config.templates_dest))
    .pipe($.size({title: 'Views'}))
    .pipe(server.stream())
};

const fontawesome = () => {
  return gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
    .pipe(gulp.dest(config.fonts_dest));
};

const fonts = () => {
  return gulp.src(config.fonts_src)
    .pipe(gulp.dest(config.fonts_dest));
};

const images = () => {
  return gulp.src(config.img_src)
    .pipe($.imagemin())
    .pipe(gulp.dest(config.img_dest))
};

const videos = () => {
  return gulp.src(config.video_src)
    .pipe($.imagemin())
    .pipe(gulp.dest(config.video_dest))
};


const data = () => {
  return gulp.src(config.data_src)
    .pipe(gulp.dest(config.data_dest))
};

/*Copy the content from src/ to dist*/
gulp.task('copy', () => {
  $.plumber()
    .pipe($.size({title: 'Copying ./src content in ./dist'}))
    .pipe(gulp.dest(config.dist_folder));
});

const reload = () => browserSync.reload;

const serve = gulp.series(clean, styles, vendor, scripts, views, fonts, fontawesome, images, videos, data, (done) => {
  const startTime = Date.now();
  console.log('\x1b[42m************************************\x1b[0m\n');
  console.log('\x1b[32m  Project ready for coding 😎\x1b[0m\n');
  console.log('\x1b[42m************************************\x1b[0m\n');
  console.log('[\x1b[32m\x1b[0m]', `All finished in \x1b[35m${Date.now() - startTime} ms`, '\x1b[0m\n');
  server.init({
    notify: config.notify,
    injectChanges: true,

    server: {
      baseDir: "./dist",
      livereload: true
    },
    port: config.port

  });


  done();
});

if (argv.watch) {
  gulp.watch(['./app/css/**/*.scss'], styles, reload);
  gulp.watch('./app/templates/includes/**/*.pug', views, reload);
  gulp.watch(['./app/js/**/*.js'], scripts, reload);
  gulp.watch(['./app/img/!**!/!*'], reload);
}

gulp.task('production', gulp.series(clean, styles, vendor, scripts, views, fonts, fontawesome, images, videos, (done) => {
  const startTime = Date.now();
  console.log('[\x1b[32m\x1b[0m]', `All finished in \x1b[35m${Date.now() - startTime} ms`, '\x1b[0m\n');
  done();
}));

const dev = gulp.series(clean, serve, () => {
  const startTime = Date.now();
  console.log('[\x1b[32m\x1b[0m]', `All finished in \x1b[35m${Date.now() - startTime} ms`, '\x1b[0m\n');
});

export default dev;

